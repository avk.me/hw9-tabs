const tabs = document.querySelectorAll('.tabs-title');
const activeTabs = document.getElementsByClassName('active-tab');
const activeElements = document.getElementsByClassName('active-element');
const contentElements = document.querySelectorAll('.tabs-content li');

for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', function (event) {
        activeTabs[0].classList.remove('active-tab');
        activeElements[0].classList.remove('active-element');
        event.target.classList.add('active-tab');
        if (contentElements[i].dataset.tab === activeTabs[0].innerHTML) {
            contentElements[i].classList.add('active-element');
        }
    })
}